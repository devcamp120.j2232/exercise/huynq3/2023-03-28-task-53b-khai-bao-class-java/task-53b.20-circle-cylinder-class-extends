import com.devcamp.Circle;
import com.devcamp.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        // sub 1: khai bao 3 đối tượng Cirle voi tham số các kiểu
        Circle circle1= new Circle();
        Circle circle2= new Circle(2.0);
        Circle circle3= new Circle(3.0, "green");
        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle3);

        System.out.println("Diện tích circle1= "+circle1.getArea());
        System.out.println("Diện tích circle2= "+circle2.getArea());
        System.out.println("Diện tích circle3= "+circle3.getArea());
    //sub 5 khai bao 4 kiểu Cylinder

    Cylinder cylinder1= new Cylinder();
    Cylinder cylinder2= new Cylinder(2.5);
    Cylinder cylinder3= new Cylinder(3.5, 1.5);
    Cylinder cylinder4= new Cylinder(3.5 , "green", 1.5);

    System.out.println(cylinder1);
    System.out.println(cylinder2);
    System.out.println(cylinder3);
    System.out.println(cylinder4);

    System.out.println("Diện tích trụ 1= "+cylinder1.getVolume());
    System.out.println("Diện tích trụ 2= "+cylinder2.getVolume());
    System.out.println("Diện tích trụ 3= "+cylinder3.getVolume());
    System.out.println("Diện tích trụ 4= "+cylinder4.getVolume());

    }
}
